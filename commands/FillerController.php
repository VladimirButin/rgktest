<?php

namespace app\commands;

use yii\console\Controller;
use Faker\Factory;
use app\models\Author;
use app\models\Book;

/**
 * This controller is used for filling tables with random junk
 */
class FillerController extends Controller
{

    /**
     * Generates random authors and add them to table `authors`
     * @param int $count number of rows to add
     */
    public function actionAuthors($count = 0)
    {
        if ($count > 0) {
            $factory = \Faker\Factory::create();

            for ($number = 0; $number < $count; $number++) {
                $author = new Author;

                $author->attributes = [
                    'firstname' => $factory->firstName,
                    'lastname' => $factory->lastName,
                ];
                echo $author->firstname . ' ' . $author->lastname . "\n";

                $author->save();
            }
        }
    }

    /**
     * Generates random books and add them to table `books`
     * @param int $count number of rows to add
     */
    public function actionBooks($count = 0)
    {
        if ($count > 0) {
            $factory = \Faker\Factory::create();
            $authorsIds = Author::getAllAuthorIds();

            for ($number = 0; $number < $count; $number++) {
                $book = new Book;
                $book->date = $factory->dateTime->format('Y-m-d H:i:s');
                $book->name = $factory->company;
                $book->preview = 'foobarbaz';
                $book->author_id = $factory->randomElement($authorsIds);

                echo $book->name . ' ' . $book->preview . ' ' . $book->date . ' ' . $book->author_id . "\n";

                $book->save();
            }
        }
    }

}
