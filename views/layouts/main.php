<?php
/* @var $this \yii\web\View */
/* @var $content string */
/* @var $loginForm \app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'Test application',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' =>
                Yii::$app->user->isGuest ?
                        [
                    [
                        'label' => 'Registration',
                        'linkOptions' => [
                            'data-target' => '#registrationForm',
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Login',
                        'linkOptions' => [
                            'data-target' => '#loginForm',
                            'data-toggle' => 'modal',
                        ],
                    ]
                        ] :
                        [
                    [
                        'label' => 'Books',
                        'url' => ['/books/index'],
                    ],
                    [
                        'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ],
                        ]
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?= $content ?>
            </div>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
