<?php
/* @var $this \yii\web\View */
/* @var $loginForm \app\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

$modal = Modal::begin([
            'id' => 'registrationForm',
            'header' => 'Registration form',
        ]);

$form = ActiveForm::begin([
            'action' => ['site/registration'],
            'method' => 'POST',
        ]);

// Login field
echo $form->field($registrationForm, 'username', [
    'inputOptions' => [
        'placeholder' => 'Enter your username',
    ]
]);

// Password field
echo $form->field($registrationForm, 'password', [
    'inputOptions' => [
        'placeholder' => 'Enter your password',
        'type' => 'password',
    ]
]);
?>

<div class="form-group">
    <?= Html::submitButton('Register', ['class' => 'btn btn-primary']); ?>
    <?= Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
</div>

<?php
ActiveForm::end();
Modal::end();
