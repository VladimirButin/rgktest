<?php
/* @var $this yii\web\View */

use yii\bootstrap\Alert;

$this->title = 'RGK Test Application';
?>

<?php
if (Yii::$app->user->isGuest) {
    echo $this->render('_registration', [
        'registrationForm' => $registrationForm,
    ]);

    echo $this->render('_login', [
        'loginForm' => $loginForm,
    ]);
}
?>
<div class="site-index">
    <div class="body-content">
        <?php
        if (Yii::$app->getSession()->hasFlash('login')) {

            echo Alert::widget([
                'options' => [
                    'class' => 'alert-danger',
                ],
                'body' => Yii::$app->session->getFlash('login')
            ]);
        }
        if (Yii::$app->getSession()->hasFlash('registration')) {

            echo Alert::widget([
                'options' => [
                    'class' => 'alert-danger',
                ],
                'body' => Yii::$app->session->getFlash('registration')
            ]);
        }
        ?>
    </div>
</div>
