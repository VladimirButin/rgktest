<?php
/* @var $this \yii\web\View */
/* @var $loginForm \app\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

$modal = Modal::begin([
            'id' => 'loginForm',
            'header' => 'Login form',
        ]);
$form = ActiveForm::begin([
            'action' => ['site/login'],
            'method' => 'POST',
        ]);

// Login field
echo $form->field($loginForm, 'username', [
    'inputOptions' => [
        'placeholder' => 'Enter your username',
    ]
]);

// Password field
echo $form->field($loginForm, 'password', [
    'inputOptions' => [
        'placeholder' => 'Enter your password',
        'type' => 'password',
    ]
]);
?>

<div class="form-group">
    <?= Html::submitButton('Login', ['class' => 'btn btn-primary']); ?>
    <?= Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
</div>

<?php
ActiveForm::end();
Modal::end();
