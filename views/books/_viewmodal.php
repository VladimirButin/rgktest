<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

Modal::begin([
    'id' => 'viewForm',
    'header' => 'View form',
]);
?>

<div class="view-form-content">

</div>

<div class="form-group">
    <?= Html::button('Cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
</div>

<?php
Modal::end();
