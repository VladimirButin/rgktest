<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\BookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
Pjax::begin([
    'id' => 'booksSearchPjaxForm',
    'options' => ['class' => 'book-search']
])
?>
<?php
$form = ActiveForm::begin([
            'id' => 'booksSearchForm',
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => true,
            ]
        ]);
?>

<?= $form->field($model, 'name') ?>

<?= $form->field($model, 'author_id')->dropDownList(["" => ""] + $authors) ?>

<?= $form->field($model, 'date_from')->input('text', ['placeholder' => 'YYYY-MM-DD HH:MM:SS']) ?>

<?= $form->field($model, 'date_to')->input('text', ['placeholder' => 'YYYY-MM-DD HH:MM:SS']) ?>

<div class="form-group">
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>
