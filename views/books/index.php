<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
?>

<?php echo $this->render('_viewmodal'); ?>

<div class="book-index">
    <p>
        <?= Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>  

    <?php echo $this->render('_search', ['model' => $searchModel, 'authors' => $authors]); ?>

    <?php Pjax::begin(['id' => 'booksGrid']); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'preview',
            'author',
            'date',
            'date_create',
            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'class' => 'full-view',
                                    'title' => 'View',
                                    'data-bookid' => $model->id,
                                    'data-url' => Url::to(['books/view', 'id' => $model->id]),
                                    'data-pjax' => 0,
                        ]);
                    },
                            'update' => function($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'class' => 'edit-view',
                                    'title' => 'Edit',
                                    'data-bookid' => $model->id,
                                    'data-url' => Url::to(['books/update', 'id' => $model->id]),
                                    'data-pjax' => 0,
                        ]);
                    }
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>

</div>
