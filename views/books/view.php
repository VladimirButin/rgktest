<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = 'View Book: ' . $model->name;
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'date_create',
            'date_update',
            'preview',
            'date',
            [
                'label' => 'Author',
                'value' => $authors[$model->author_id],
            ],
        ],
    ])
    ?>

</div>

<?php
$this->registerJs('
    if (window.opener) {
        window.opener.$(window.opener.document).trigger("book-edit");
    }
');
