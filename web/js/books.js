/* 
 * Here lies various JS code snippets
 */


$(document).ready(function () {
    // Edit handler
    $(document).on("click", "a.edit-view", function (e) {
        e.preventDefault();
        var popup = window.open($(this).data("url"));
        $(document).one("book-edit", function () {
            popup.close();
            $.pjax.reload({container: "#booksGrid"});
        });
    });

    // View handler
    $(document).on("click", "a.full-view", function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).data("url"),
        }).done(function (data) {
            $("#viewForm .view-form-content").html(data);
            $("#viewForm").modal();
        });
    })

    // PJAX Search Form handler
    $("#booksSearchPjaxForm").on("pjax:end", function () {
        $.pjax.reload({container: "#booksGrid"});
    });
});