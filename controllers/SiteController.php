<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegistrationForm;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
                    'loginForm' => new LoginForm,
                    'registrationForm' => new RegistrationForm,
        ]);
    }

    public function actionRegistration()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $registrationForm = new RegistrationForm();
        if ($registrationForm->load(Yii::$app->request->post()) && $registrationForm->validate()) {
            Yii::$app->user->login($registrationForm->register());
            $this->goHome();
        }

        Yii::$app->getSession()->setFlash('registration', 'An error occurred during registration process');
        return $this->render('index', [
                    'loginForm' => new LoginForm,
                    'registrationForm' => $registrationForm,
        ]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $loginForm = new LoginForm();
        if ($loginForm->load(Yii::$app->request->post()) && $loginForm->validate()) {
            Yii::$app->user->login($loginForm->getUser());
            $loginForm->getUser()->save();
            $this->goHome();
        }

        Yii::$app->getSession()->setFlash('login', 'An error occurred during login process');
        return $this->render('index', [
                    'loginForm' => $loginForm,
                    'registrationForm' => new RegistrationForm,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
