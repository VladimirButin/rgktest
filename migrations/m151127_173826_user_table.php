<?php

use yii\db\Schema;
use yii\db\Migration;

class m151127_173826_user_table extends Migration
{

    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(100)->notNull()->unique(),
            'password' => $this->string(100)->notNull(),
            'createdAt' => $this->dateTime()->notNull(),
            'operatedAt' => $this->timestamp(),
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
