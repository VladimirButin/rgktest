<?php

use yii\db\Schema;
use yii\db\Migration;

class m151129_165038_author_table extends Migration
{

    public function up()
    {
        $this->createTable('authors', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string(100)->notNull(),
            'lastname' => $this->string(100)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('authors');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
