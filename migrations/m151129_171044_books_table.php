<?php

use yii\db\Schema;
use yii\db\Migration;

class m151129_171044_books_table extends Migration
{

    public function up()
    {
        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'date_create' => $this->dateTime(),
            'date_update' => $this->timestamp(),
            'preview' => $this->string(300),
            'date' => $this->dateTime(),
            'author_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('books');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
