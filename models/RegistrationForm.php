<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;
use app\models\UserForm;

/**
 * Registration form class
 */
class RegistrationForm extends UserForm
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return array_merge(parent::rules(), [['username', 'validateUsername']]);
    }

    /**
     * Validates the username.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = User::findByUsername($this->username);

            if (!empty($user)) {
                $this->addError($attribute, 'User is already existing');
            }
        }
    }

    /**
     * Registering a user
     * @return User
     */
    public function register()
    {
        $user = new User;
        $user->attributes = [
            'username' => $this->username,
            'password' => Yii::$app->getSecurity()->generatePasswordHash($this->password),
        ];
        $user->save();

        return $user;
    }

}
