<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book;

/**
 * BookSearch represents the model behind the search form about `app\models\Book`.
 * 
 * @property string $date_from
 * @property string $date_to
 * @property integer $author_id
 * @property string $name
 */
class BookSearch extends Model
{

    public $author_id;
    public $name;
    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id'], 'validateAuthorId'],
            [['date_from', 'date_to'], 'date', 'format' => 'yyyy-M-d H:m:s'],
            [['name', 'date_from', 'date_to', 'author_id'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'author_id' => 'Author ID',
            'date_from' => 'Book came out no earlier than',
            'date_to' => 'Book came out no later than',
            'name' => 'Name',
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Add all the parameters
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    protected function addParameters($query)
    {
        if (!empty($this->author_id)) {
            $query->andFilterWhere(['author_id' => $this->author_id]);
        }

        if (!empty($this->date_from)) {
            $query->andFilterWhere(['>=', 'date', $this->date_from]);
        }

        if (!empty($this->date_to)) {
            $query->andFilterWhere(['<=', 'date', $this->date_to]);
        }

        if (!empty($this->name)) {
            $query->andFilterWhere(['like', 'name', $this->name]);
        }
        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        $dataProvider = new ActiveDataProvider([
            'pagination' => [
                'pageSize' => Yii::$app->params['pageSize'],
            ],
            'query' => $query,
        ]);

        $dataProvider->getSort()->attributes += ['author' => [
                'asc' => ['author' => SORT_ASC],
                'desc' => ['author' => SORT_DESC],
                'label' => 'Author',
                'default' => SORT_ASC,
        ]];
        $query->select(['{{books}}.*', 'CONCAT({{authors}}.[[firstname]]," ",{{authors}}.[[lastname]]) AS author'])
                ->innerJoin('authors', '{{authors}}.[[id]] = {{books}}.[[author_id]]');

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0 = 1');
            return $dataProvider;
        }
        $this->addParameters($query);

        return $dataProvider;
    }

    /**
     * Validation author id
     * @param type $authorId
     */
    public function validateAuthorId($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $authorId = $this->{$attribute};
            if ($authorId === '') {
                return true;
            }

            $author = Author::findOne(['id' => $authorId]);
            if (empty($author)) {
                $this->addError($attribute, 'Incorrect author');
            }
        }
    }

}
