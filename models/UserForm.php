<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Basis for login and registration forms
 */
class UserForm extends Model
{

    public $username;
    public $password;
    protected $user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'password' => 'Password',
        ];
    }

    /**
     * Getting user property
     * @return type
     */
    public function getUser()
    {
        if ($this->user == false) {
            $this->user = User::findByUsername($this->username);
        }

        return $this->user;
    }

}
