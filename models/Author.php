<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 */
class Author extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['firstname', 'lastname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'First name',
            'lastname' => 'Last name',
        ];
    }

    /**
     * Getting all them authors
     * @return array
     */
    public static function getAllAuthors()
    {
        $authors = static::find()->all();

        $authorsArray = [];
        foreach ($authors as $author) {
            $authorsArray[$author->id] = $author->firstname . ' ' . $author->lastname;
        }

        return $authorsArray;
    }

    /**
     * Getting all them authors ids
     * @return array
     */
    public static function getAllAuthorIds()
    {
        return static::find()->select(['id'])->column();
    }

}
