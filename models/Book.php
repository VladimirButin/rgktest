<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $preview
 * @property string $date
 * @property string $author
 * @property integer $author_id
 */
class Book extends \yii\db\ActiveRecord
{

    public $author;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['date_create', 'date_update', 'date'], 'safe'],
            [['author_id'], 'validateAuthorId'],
            [['name'], 'string', 'max' => 100],
            [['preview'], 'string', 'max' => 300],
            ['author', 'string'],
            ['date', 'date', 'format' => 'yyyy-M-d H:m:s'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date_create' => 'Date Create',
            'date_update' => 'Date Update',
            'preview' => 'Preview',
            'date' => 'Date',
            'author_id' => 'Author ID',
            'author' => 'Author',
        ];
    }

    /**
     * @inheritdoc
     * @return BookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->date_create = date("Y-m-d H:i:s");
            } else {
                $this->date_update = date("Y-m-d H:i:s");
            }
            return true;
        }
        return false;
    }

    /**
     * Get author property
     * @return type
     */
    public function getAuthor()
    {
        if (empty($this->author)) {
            $authorRow = Author::findOne(['id' => $this->author_id]);
            $this->author = $authorRow->firstname . ' ' . $authorRow->lastname;
        }
        return $this->author;
    }

    /**
     * Validation author id
     * @param type $authorId
     */
    public function validateAuthorId($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $authorId = $this->{$attribute};

            $author = Author::findOne(['id' => $authorId]);
            if (empty($author)) {
                $this->addError($attribute, 'Incorrect author');
            }
        }
    }

}
